#ifndef LISTA
#define LISTA

typedef struct list
{
  int size;
  struct node *head;
  struct node *tail;
} list_t;

typedef struct node
{
  int value;
  struct node *next;
  struct node *prev;
} node_t; 

void destroy_list(list_t *lista);

void insert_head(list_t *lista, int num);

void insert_tail(list_t *lista, int num);

void insert_to_list(list_t *lista1, list_t *lista2, FILE *numbers);

void init_list(list_t *lista);

#endif
#include <stdio.h>
#include <stdlib.h>
#include "lista.h"


void destroy_list(list_t *lista)
{
  node_t *current;
  node_t *next;
  current = lista->head;
  while(current)
  {
    next = current->next;
    free(current);
    current = next;
  }
  lista->head = NULL;
}


int char_to_int(char c) //convert char type to integer type
{
  int aux;
  aux = c - 48; 
  return(aux);
}

void init_list(list_t *lista) //list initialization
{
  lista->head = lista->tail = NULL;
  lista->size = 0;
}

void insert_head(list_t *lista, int num) //insert from head to tail
{
  node_t *node;
  node = (node_t *)malloc(sizeof(node_t));
  node->prev = node->next = NULL;
  node->value = num;  
  if((lista->head == NULL) && (lista->tail == NULL))
  {
    lista->head = node;
    lista->tail = node;
  }
  else
  {
    lista->tail->next = node;
    node->prev = lista->tail;
    lista->tail = node;
  }
  lista->size++;
}

void insert_tail(list_t *lista, int num) //insert from tail to head
{
  node_t *node;
  node = (node_t *)malloc(sizeof(node_t));
  node->prev = node->next = NULL;
  node->value = num;
  if((lista->head == NULL) && (lista->tail == NULL))
  {
    lista->head = lista->tail = node;
  }
  else
  {
    lista->head->prev = node;
    node->next = lista->head;
    lista->head = node;
  }
  lista->size++;
}


void insert_to_list(list_t *lista1, list_t *lista2, FILE *numbers) //insert numbers to 2 lists
{
  char c;
  int aux;
  c = getc(numbers);
  aux = char_to_int(c);
  while(((c >= '0') && (c <= '9')) || (c == ' '))
  { 
    if (c == ' ')
    { 
      c = getc(numbers);
      aux = char_to_int(c);
      while((c >= '0') && (c <= '9'))
      {
        insert_head(lista2, aux);
        c = getc(numbers);
        aux = char_to_int(c);
      }
    }
    else
    {
      insert_head(lista1, aux);
      c = getc(numbers);
      aux = char_to_int(c);    
    }
  }
  if(c != EOF)
  {
    if(c < '0' || c > '9')
    {
      printf("Formato invalido.\n");
      exit(1);
      destroy_list(lista1);
      destroy_list(lista2);
    }
  }

  fclose(numbers);
}
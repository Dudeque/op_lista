#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

void subtrai(list_t *lista1, list_t *lista2, list_t *lista3)
{
  node_t *no1, *no2;
  int sub = 0;
  no1 = (node_t *)malloc(sizeof(node_t));
  no2 = (node_t *)malloc(sizeof(node_t));
  no1 = lista1->tail;
  no2 = lista2->tail;
  while(no1 && no2)
  {
    if(no2->value > no1->value)
    {
      no1->value += 10;
      no1->prev->value--;
    }
    sub = no1->value - no2->value;
    insert_tail(lista3, sub);
    sub = 0;
    no1 = no1->prev;
    no2 = no2->prev;
  }
  if(no1)
  {
    if(no1->value < 0) 
    {
      no1->value += 10;
      no1->prev->value--;
    }
    while(no1)
    {
      insert_tail(lista3, no1->value);
      no1 = no1->prev;
    }
  }
}

int main(int argc, char **argv)
{
  list_t *lista1, *lista2, *lista3;
  node_t *node1;
  lista1 = malloc(sizeof(list_t));
  lista2 = malloc(sizeof(list_t));
  lista3 = malloc(sizeof(list_t));
  init_list(lista1);
  init_list(lista2);
  init_list(lista3);
  FILE *result = fopen("ResultadoSub.txt", "w");
  FILE *numbers = fopen(argv[1], "r");
  if(!numbers)
  {
    perror("Arquivo invalido.");
    exit(1);
  }
  insert_to_list(lista1, lista2, numbers);
  if(lista1->size > lista2->size)
    subtrai(lista1, lista2, lista3);
  else if (lista1->size == lista2->size)
  {
    node_t *n1, *n2;
    n1 = (node_t *)malloc(sizeof(node_t));
    n2 = (node_t *)malloc(sizeof(node_t));
    n1 = lista1->head;
    n2 = lista2->head;  
    while(n1 && n2)
    {
      if(n1->value != n2->value)
        break;
      n1 = n1->next;
      n2 = n2->next;
    }
    if(!n1 || !n2)
    {        
      subtrai(lista1, lista2, lista3);
    }
    else if(n1->value > n2->value)
    {
      subtrai(lista1, lista2, lista3);
    }
    else
    {      
      subtrai(lista2, lista1, lista3);
      fprintf(result, "-");
    }  
  }
  else
  {
    subtrai(lista2, lista1, lista3);
    fprintf(result, "-");
  }  
  node1 = lista3->head;
  while(node1)
  {
    fprintf(result, "%d", node1->value);
    node1 = node1->next;    
  }
  destroy_list(lista1);
  destroy_list(lista2);
  destroy_list(lista3);
}
#include <stdio.h>
#include <stdlib.h>
#include "lista.h"


void soma_parcial(list_t *lista1, list_t *lista2)
{
  node_t *no1, *no2;
  int soma = 0, vai1 = 0;
  no1 = (node_t *)malloc(sizeof(node_t));
  no2 = (node_t *)malloc(sizeof(node_t));
  no1 = lista1->tail;
  no2 = lista2->tail;
  
  while(no1 && no2)
  { 
    if(vai1)
      soma++;    
    soma += no1->value + no2->value;    
    if(soma > 9)
    {
      soma = soma % 10;
      vai1 = 1;
    }
    else
    {
      vai1 = 0;
    }    
    no2->value = soma;    
    soma = 0;
    no1 = no1->prev;
    no2 = no2->prev;
  }
  if(no1)
  {
    if(vai1)
    {
      no1->value++;
      vai1 = 0;
    }
    while(no1)
    {
      insert_tail(lista2, no1->value);
      no1 = no1->prev;
    }
  }
  if(vai1)
    insert_tail(lista2, 1);
}

void multiplica(list_t *lista1, list_t *lista2, list_t *lista3)
{
  node_t *no1, *no2;
  int mult = 0, vai = 0, count = 0;
  list_t *lista4;
  lista4 = malloc(sizeof(list_t));  
  no1 = (node_t *)malloc(sizeof(node_t));
  no2 = (node_t *)malloc(sizeof(node_t));
  no1 = lista1->tail;
  no2 = lista2->tail;
  while(no2)
  {
    init_list(lista4);
    while(no1)
    {
      mult = ((no1->value * no2->value) % 10) + vai;
      vai = (no1->value * no2->value) / 10;
      no1 = no1->prev;
      insert_tail(lista4, mult);
    }
    no1 = lista1->tail;
    if(vai)
      insert_tail(lista4, vai);
    vai = 0;
    for (int i = 0; i < count; ++i)
    {
      insert_head(lista4, 0);
    }
    soma_parcial(lista4, lista3);
    destroy_list(lista4);
    count++;    
    no2 = no2->prev;
  }
} 


int main(int argc, char **argv)
{
  list_t *lista1, *lista2, *lista3;
  node_t *node1;
  lista1 = malloc(sizeof(list_t));
  lista2 = malloc(sizeof(list_t));
  lista3 = malloc(sizeof(list_t));
  init_list(lista1);
  init_list(lista2);
  init_list(lista3);
  FILE *result = fopen("ResultadoMul.txt", "w");
  FILE *numbers = fopen(argv[1], "r");
  if(!numbers)
  {
    perror("Arquivo invalido.");
    exit(1);
  }
  insert_to_list(lista1, lista2, numbers);
  multiplica(lista1, lista2, lista3);
  node1 = lista3->head;
  while(node1)
  {
    fprintf(result, "%d", node1->value);
    node1 = node1->next;    
  }
  destroy_list(lista1);
  destroy_list(lista2);
  destroy_list(lista3);
}